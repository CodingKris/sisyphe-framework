Sisyphe-Framework-Monitor-Runtime 执行时间监控
===
对方法的执行时间进行监控并记录到日志中，并可通过kafka传入到ElasticSearch中进行监控。
目前可实现controller和jpa相关的监控，相关配置可参考 MethodMonitor 下的 @Pointcut 注解
 
 
### 更新说明
版本|时间|更新内容
:---: |   --- |   :---: 
1.0 |   1. 增加MethodMonitor 方法监控 | 2017-11-03

### monitor-runtime 接口层说明
    - MethodMonitor   方法执行时间监控类
    - MethodRunTime   执行时间日志收集类
    - MonitorTypeEnum   监控方式枚举
        - EnableMonitor 开启监控注解
    - spring.factories  配置自动加载的类
    
### 如何调用
1.在项目中引入监控包

```
<dependency>
    <groupId>cn.sisyphe.framework</groupId>
    <artifactId>framework-monitor-runtime</artifactId>
    <version>1.0</version>
</dependency>
```

2.在启动类上开启监控注解 **@EnableMonitor** 

3.在配置文件（application.properties）中启动监控
```
sisyphe.monitor.jpa.enable=true
sisyphe.monitor.controller.enable=true
```