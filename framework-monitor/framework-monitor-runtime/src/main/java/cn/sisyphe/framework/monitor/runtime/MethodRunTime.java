package cn.sisyphe.framework.monitor.runtime;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by heyong on 2017/11/1 15:38
 * Description: 日志收集类
 * @author heyong
 */
public class MethodRunTime implements Serializable {
    private long startTime;
    private long totalTime;
    private String methodName;
    private MonitorTypeEnum monitorType;
    private String application;

    MethodRunTime(String methodName, MonitorTypeEnum monitorType, String application) {
        this.startTime = System.currentTimeMillis();
        this.methodName = methodName;
        this.monitorType = monitorType;
        this.application = application;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public MonitorTypeEnum getMonitorType() {
        return monitorType;
    }

    public void setMonitorType(MonitorTypeEnum monitorType) {
        this.monitorType = monitorType;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    @Override
    public String toString() {
        return "MethodRunTime{" +
                "startTime=" + startTime +
                ", totalTime=" + totalTime +
                ", methodName='" + methodName + '\'' +
                ", monitorType=" + monitorType +
                ", application='" + application + '\'' +
                '}';
    }
}