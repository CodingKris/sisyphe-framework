package cn.sisyphe.framework.monitor.runtime;

/**
 * Created by heyong on 2017/11/1 15:40
 * Description: 监控的类型
 * @author heyong
 */
public enum MonitorTypeEnum {

    /**
     * CONTROLLER
     */
    CONTROLLER,
    /**
     * JPA
     */
    JPA
}
