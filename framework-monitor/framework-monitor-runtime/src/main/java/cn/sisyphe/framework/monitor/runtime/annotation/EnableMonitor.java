package cn.sisyphe.framework.monitor.runtime.annotation;

import cn.sisyphe.framework.common.bootstrap.AutoConfigurationImportSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Created by heyong on 2017/10/24 12:27
 * Description: 开启监控
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
@Documented
@Import(AutoConfigurationImportSelector.class)
public @interface EnableMonitor {

}
