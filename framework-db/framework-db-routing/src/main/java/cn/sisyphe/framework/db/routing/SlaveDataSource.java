package cn.sisyphe.framework.db.routing;

import javax.sql.DataSource;

/**
 * Created by heyong on 2017/12/5 10:32
 * Description:
 * @author heyong
 */
public interface SlaveDataSource {

    /**
     * 同步库
     * @return
     */
    DataSource slaveDataSource();
}
