package cn.sisyphe.framework.db.routing;

/**
 * Created by heyong on 2017/12/4 18:37
 * Description: 数据库切换器
 * @author heyong
 */
public class DynamicDataSourceHolder {

    /**
     * 使用ThreadLocal把数据源与当前线程绑定
     */
    private static final ThreadLocal<DbType> DATA_SOURCES = new ThreadLocal<DbType>();

    public static void setDataSource(DbType dbType) {
        if (dbType == null){
            dbType = DbType.MASTER;
        }

        DATA_SOURCES.set(dbType);
    }

    public static DbType getDataSource() {
        return DATA_SOURCES.get() == null ? DbType.MASTER : DATA_SOURCES.get();
    }
    public static void clearDataSource() {
        DATA_SOURCES.remove();
    }
}

