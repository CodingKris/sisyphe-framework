package cn.sisyphe.framework.db.routing;

import javax.sql.DataSource;

/**
 * Created by heyong on 2017/12/5 10:29
 * Description:
 * @author heyong
 */
public interface MasterDataSource {

    /**
     * 主库
     * @return
     */
    DataSource masterDataSource();
}
