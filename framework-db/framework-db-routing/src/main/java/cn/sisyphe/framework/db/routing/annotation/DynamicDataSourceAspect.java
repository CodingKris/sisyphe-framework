package cn.sisyphe.framework.db.routing.annotation;

import cn.sisyphe.framework.db.routing.DynamicDataSourceHolder;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

/**
 * Created by heyong on 2017/12/4 18:40
 * Description:
 *
 * @author heyong
 */
@Aspect
@Component
public class DynamicDataSourceAspect implements Ordered {

    private static final Logger logger = LoggerFactory.getLogger(DynamicDataSourceAspect.class);

    @Around("@annotation(targetDataSource)")
    public Object proceed(ProceedingJoinPoint pjp, TargetDataSource targetDataSource) throws Throwable {

        try {
            logger.info("切换数据库是：{}",  targetDataSource.value());

            DynamicDataSourceHolder.setDataSource(targetDataSource.value());
            return pjp.proceed();
        } finally {
            DynamicDataSourceHolder.clearDataSource();
        }
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
