package cn.sisyphe.framework.db.routing.annotation;

import cn.sisyphe.framework.db.routing.DbType;

import java.lang.annotation.*;

/**
 * Created by heyong on 2017/12/4 18:39
 * Description:
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TargetDataSource {
    DbType value() default DbType.MASTER;
}
