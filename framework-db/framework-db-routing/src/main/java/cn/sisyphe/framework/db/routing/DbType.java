package cn.sisyphe.framework.db.routing;

/**
 * Created by heyong on 2017/12/5 10:53
 * Description:
 * @author heyong
 */
public enum DbType {

    /**
     * 主库
     */
    MASTER,
    /**
     * 子库
     */
    SLAVE
}
