Sisyphe-Framework 1.0 框架说明
===
Sisyphe-Framework 在Spring Boot的基础上，轻量集成开发过程中的常用模块，做到灵活、简单、快速。

### 更新说明
版本|更新内容| 时间
--- | ---   | --- 
1.0 | 1. 支持StopWatch注解方式<br>2. 缓存支持redis<br>3. 消息支持rabbitMQ    | 2017-10-24
1.0 | 1. 运行时间监控monitor  | 2017-11-03
1.0 | 1. 增加统一日志处理   | 2017-11-22
1.0 | 1. 增加权限范围过滤   | 2017-11-24
1.0 | 1. 增加数据库读写分离  | 2017-12-05

### 框架介绍
+ [framework-common](framework-common) 公用工具类
+ [framework-auth](framework-auth) 权限工具类
    > + [framework-auth-logic](framework-auth/framework-auth-logic) 权限范围过滤，例子详见[framework-example](framework-example)
+ [framework-cache](framework-cache) 缓存工具类
    > + [framework-cache-core](framework-cache/framework-cache-core) 缓存核心模块
    > + [framework-cache-redis](framework-cache/framework-cache-redis) 采用redis作为缓存
    > + [framework-cache-sample](framework-cache/framework-cache-sample) 缓存调用示例
+ [framework-message](framework-message) 消息工具类
    > + [framework-message-core](framework-message/framework-message-core) 消息核心模块
    > + [framework-message-rabbit](framework-message/framework-message-rabbit) 采用rabbit作为消息中间件
    > + [framework-message-sample](framework-message/framework-message-sample) 消息调用示例
+ [framework-monitor](framework-monitor) 监控工具类
    > + [framework-monitor-runtime](framework-monitor/framework-monitor-runtime) 运行执行时间监控
+ [framework-db](framework-db) 数据库工具类
    > + [framework-db-routing](framework-db/framework-db-routing) 数据库动态路由（读写分离）
+ [framework-web](framework-web) 网页相关类
+ [framework-example](framework-example) 综合例子    