package cn.sisyphe.framework.example;

import java.util.List;
import java.util.Set;

/**
 * Created by heyong on 2017/11/24 15:44
 * Description:
 */
public class User {

    private String userName;
    private List<String> stationCode;
    private Set<String> stationCode1;
    private String stationCode2;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<String> getStationCode() {
        return stationCode;
    }

    public void setStationCode(List<String> stationCode) {
        this.stationCode = stationCode;
    }

    public Set<String> getStationCode1() {
        return stationCode1;
    }

    public void setStationCode1(Set<String> stationCode1) {
        this.stationCode1 = stationCode1;
    }

    public String getStationCode2() {
        return stationCode2;
    }

    public void setStationCode2(String stationCode2) {
        this.stationCode2 = stationCode2;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", stationCode=" + stationCode +
                ", stationCode1=" + stationCode1 +
                ", stationCode2='" + stationCode2 + '\'' +
                '}';
    }
}
