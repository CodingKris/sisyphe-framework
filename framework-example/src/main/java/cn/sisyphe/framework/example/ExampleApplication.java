package cn.sisyphe.framework.example;


import cn.sisyphe.framework.auth.logic.AuthenticationSupplier;
import cn.sisyphe.framework.auth.logic.annotation.EnableScopeAuth;
import cn.sisyphe.framework.auth.logic.annotation.ScopeAuth;
import cn.sisyphe.framework.cache.core.annotation.EnableS2Cache;
import cn.sisyphe.framework.web.ResponseResult;
import cn.sisyphe.framework.web.exception.DataException;
import cn.sisyphe.framework.web.exception.GlobalExceptionHandler;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by heyong on 2017/11/21 14:49
 * Description:
 *
 * @author heyong
 */
@SpringBootApplication
@RestController
@EnableSwagger2
@EnableScopeAuth
@EnableS2Cache
@Import(GlobalExceptionHandler.class)
public class ExampleApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ExampleApplication.class, args);
    }

    @Autowired
    private AuthenticationSupplier authSupplier;

    @Override
    public void run(String... strings) throws Exception {

        Set<String> scopeList = new HashSet<String>();
        scopeList.add("5555");

        // 导入权限范围
        authSupplier.importScope("hypier", scopeList);

        // 获取用户的权限范围
        // Set<String> userScopeList = authSupplier.findScope("hypier");
    }


    @ScopeAuth(scope = "#user.stationCode", token = "#user.userName")
    @ApiImplicitParam(name = "AUTH_TOKEN", value = "身份验证码", defaultValue = "9848d4408707a77cf8f74db3455f2490", paramType = "header")
    @PostMapping(value = "/get")
    public ResponseResult get(@RequestBody User user) {

        ResponseResult responseResult = new ResponseResult();
        responseResult.put("user", user);


        //throw new DataException("100", "出错了");
//        throw new DataException("1100", "112", new String[]{"C001", "SB123"});
        //throw new DataException("1100", "112", new String[]{"C001", "SB123"}, responseResult);
        return responseResult;
    }


}
