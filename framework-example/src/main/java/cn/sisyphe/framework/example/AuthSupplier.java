package cn.sisyphe.framework.example;

import cn.sisyphe.framework.auth.logic.AuthenticationSupplier;
import cn.sisyphe.framework.cache.core.CacheHelper;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by heyong on 2017/11/24 11:37
 * Description:
 *
 * @author heyong
 */
@Service
public class AuthSupplier implements AuthenticationSupplier {

    private final String REDIS_KEY = "auth_supplier_scope_";

    @Override
    public void importScope(String key, Set<String> values) {
        CacheHelper.cache().set(REDIS_KEY + key, values);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<String> findScope(String key) {

        return (Set<String>) CacheHelper.cache().get(REDIS_KEY + key);
    }
}
