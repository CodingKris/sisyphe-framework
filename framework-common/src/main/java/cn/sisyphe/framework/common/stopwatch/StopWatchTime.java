package cn.sisyphe.framework.common.stopwatch;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * Created by heyong on 2017/10/17 9:17
 * Description:
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD})
public @interface StopWatchTime {
    String value() default "";

}
