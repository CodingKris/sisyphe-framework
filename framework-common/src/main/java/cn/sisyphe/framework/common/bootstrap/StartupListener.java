package cn.sisyphe.framework.common.bootstrap;

import org.springframework.context.ApplicationContext;

/**
 * 系统启动监听
 * @author heyong
 */
public interface StartupListener {

    /**
     * 顺序
     * @return
     */
    default LoadOrder getOrder(){
        return LoadOrder.LAST;
    }

    /**
     * 初始化
     */
    default void init() {
    }

    /**
     * 完成
     * @param context
     */
    default void complete(ApplicationContext context) {
    }

    /**
     * 销毁
     */
    default void destory() {
    }

    enum LoadOrder {
        /**
         * 
         */
        FIRST,
        /**
         * 
         */
        MIDDLE,
        /**
         * 
         */
        LAST
    }
}
