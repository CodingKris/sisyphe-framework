package cn.sisyphe.framework.common.stopwatch;

import org.springframework.stereotype.Component;

/**
 * Created by heyong on 2017/10/17 12:18
 * Description:
 * @author heyong
 */
@Component
public class StopWatchStep {

    public String watch(String stepName) {
        return stepName;
    }
}
