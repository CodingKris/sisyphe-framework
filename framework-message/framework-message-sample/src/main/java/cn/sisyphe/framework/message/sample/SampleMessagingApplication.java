package cn.sisyphe.framework.message.sample;

import cn.sisyphe.framework.message.core.MessagingHelper;
import cn.sisyphe.framework.message.core.annotation.EnableS2Messaging;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * Created by heyong on 2017/10/25 12:21
 * Description:
 * @author heyong
 */
@EnableS2Messaging
@SpringBootApplication
public class SampleMessagingApplication implements CommandLineRunner {

    public static void main(String[] args) {
        new SpringApplicationBuilder(SampleMessagingApplication.class).run(args);
    }


    @Override
    public void run(String... strings) throws Exception {

        MessagingHelper.messaging().convertAndSend("cn_sisyphe_askcard", "askcard.trade.2.2.Unprocessed", new User("hypier", "CQ"));

    }



    @RabbitListener(queues = "askcard-trade")
    public void receiver(User user){
        System.err.println(user);
    }
}
