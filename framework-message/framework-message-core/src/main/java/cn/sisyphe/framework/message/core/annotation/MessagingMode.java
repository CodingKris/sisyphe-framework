package cn.sisyphe.framework.message.core.annotation;

/**
 * Created by heyong on 2017/10/25 11:39
 * Description:
 * @author heyong
 */
public enum MessagingMode {

    /**
     * rabbitmq
     */
    RABBIT,
    /**
     * kafka
     */
    KAFKA,
}
