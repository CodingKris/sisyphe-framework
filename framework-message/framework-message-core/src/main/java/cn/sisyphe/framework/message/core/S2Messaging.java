package cn.sisyphe.framework.message.core;

/**
 * Created by heyong on 2017/10/25 11:13
 * Description:
 * @author heyong
 */
public interface S2Messaging {

    /**
     * 消息名
     * @return
     */
    String getName();

    /**
     * 转换并发送消息
     * @param exchange
     * @param routingKey
     * @param payload
     */
    void convertAndSend(String exchange, String routingKey, Object payload);

    /**
     * 转换并发送消息
     * @param exchange
     * @param routingKey
     * @param payload
     * @param messageId
     */
    void convertAndSend(String exchange, String routingKey, Object payload, String messageId);
}
