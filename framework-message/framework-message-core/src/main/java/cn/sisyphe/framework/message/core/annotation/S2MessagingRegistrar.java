package cn.sisyphe.framework.message.core.annotation;

import cn.sisyphe.framework.message.core.MessagingHelper;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by heyong on 2017/10/25 11:41
 * Description: 通过注册bean器，查找注解，设置消息类型
 */
@Component
class S2MessagingRegistrar implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {

        Map<String, Object> defaultAttrs = metadata.getAnnotationAttributes(EnableS2Messaging.class.getName(), true);

        // 设置 messaging 的类型
        MessagingHelper.mode = defaultAttrs.get("value").toString();
    }
}
