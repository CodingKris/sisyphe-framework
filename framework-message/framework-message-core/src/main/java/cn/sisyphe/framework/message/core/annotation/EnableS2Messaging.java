package cn.sisyphe.framework.message.core.annotation;

import cn.sisyphe.framework.common.bootstrap.AutoConfigurationImportSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Created by heyong on 2017/10/25 11:40
 * Description:
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
@Documented
@Import(AutoConfigurationImportSelector.class)
public @interface EnableS2Messaging {

    MessagingMode value() default MessagingMode.RABBIT;
}
