//package cn.sisyphe.framework.message.core.annotation;
//
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.messaging.handler.annotation.MessageMapping;
//
//import java.lang.annotation.*;
//
///**
// * Created by heyong on 2017/11/13 17:49
// * Description:
// */
//@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE })
//@Retention(RetentionPolicy.RUNTIME)
//@MessageMapping
//@Documented
//@Repeatable(RabbitListener.class)
//public @interface S2MessagingListener {
//
//}
