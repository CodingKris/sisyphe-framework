package cn.sisyphe.framework.message.core;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by heyong on 2017/10/25 11:24
 * Description:
 * @author heyong
 */
@Component
public class MessagingHelper implements ApplicationContextAware {

    public static String mode = "rabbit";

    private static Map<String, S2Messaging> messagingMap;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Map<String, S2Messaging> map = applicationContext.getBeansOfType(S2Messaging.class);

        messagingMap = new HashMap<>(16);

        map.forEach((key, value) -> messagingMap.put(value.getName().toLowerCase(), value));
    }

    public static S2Messaging messaging()throws ClassNotFoundException {
        S2Messaging s2Messaging = messagingMap.get(mode.toLowerCase());

        if (s2Messaging == null) {
            throw new ClassNotFoundException("Messaging Class Not Found");
        }

        return s2Messaging;
    }
}
