package cn.sisyphe.framework.message.rabbit;

import cn.sisyphe.framework.message.core.S2Messaging;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Created by heyong on 2017/10/25 11:49
 * Description:
 * @author heyong
 */
@Component
public class RabbitMessaging implements S2Messaging {

    @Autowired
    private RabbitMessagingTemplate rabbitMessagingTemplate;

    @Override
    public String getName() {
        return "rabbit";
    }

    @Override
    public void convertAndSend(String exchange, String routingKey, Object payload) {
        convertAndSend(exchange, routingKey, payload, null);
    }

    @Override
    public void convertAndSend(String exchange, String routingKey, Object payload, String messageId) {
        if (StringUtils.isEmpty(messageId)) {
            rabbitMessagingTemplate.convertAndSend(exchange, routingKey, payload);
        } else {
            rabbitMessagingTemplate.getRabbitTemplate().convertAndSend(exchange, routingKey, payload, new CorrelationData(messageId));
        }
    }
}
