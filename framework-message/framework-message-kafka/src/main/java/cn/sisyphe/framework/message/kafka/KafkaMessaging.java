package cn.sisyphe.framework.message.kafka;

import cn.sisyphe.framework.message.core.S2Messaging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by heyong on 2017/11/13 16:28
 * Description:
 * @author heyong
 */
@Component
public class KafkaMessaging implements S2Messaging {

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Override
    public String getName() {
        return "kafka";
    }

    @Override
    public void convertAndSend(String exchange, String routingKey, Object payload) {
        kafkaTemplate.send(exchange, routingKey, payload);
    }

    @Override
    public void convertAndSend(String exchange, String routingKey, Object payload, String messageId) {

        kafkaTemplate.send(exchange, routingKey, payload);
    }
}
