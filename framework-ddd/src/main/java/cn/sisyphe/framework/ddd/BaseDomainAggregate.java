package cn.sisyphe.framework.ddd;



import cn.sisyphe.framework.ddd.annotations.DomainAggregate;

import java.io.Serializable;

/**
 * base domain aggregate
 * 领域聚合根基类
 * @author linux_china
 */
@DomainAggregate
public abstract class BaseDomainAggregate<K extends Serializable> implements BaseModel {
    /**
     * get aggregate root
     *
     * @return root object
     */
    public abstract K getRoot();

    /**
     * get entity id
     *
     * @return entity id
     */
    public K getId() {
        return getRoot();
    }
}
