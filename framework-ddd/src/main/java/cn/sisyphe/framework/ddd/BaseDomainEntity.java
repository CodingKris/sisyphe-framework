package cn.sisyphe.framework.ddd;

import cn.sisyphe.framework.ddd.annotations.DomainEntity;

import java.io.Serializable;

/**
 * base domain entity
 * 领域实体-接口
 * @author linux_china
 */
@DomainEntity
public interface BaseDomainEntity<K extends Serializable> extends BaseModel {
    /**
     * get entity id
     *
     * @return entity id
     */
    public K getId();
}
