package cn.sisyphe.framework.ddd;

import java.io.Serializable;

/**
 * base model, the parent of entity, value object and aggregate
 * 模型
 * @author linux_china
 */
public interface BaseModel extends Serializable {
}
