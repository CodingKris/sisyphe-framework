package cn.sisyphe.framework.auth.logic.annotation;

import cn.sisyphe.framework.auth.logic.AuthenticationSupplier;
import cn.sisyphe.framework.auth.logic.ScopeAuthenticationAdapter;
import cn.sisyphe.framework.common.spel.ParseSPEL;
import cn.sisyphe.framework.web.exception.AccessDeniedException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by heyong on 2017/11/23 17:54
 * Description:
 *
 * @author heyong
 */
@Aspect
@Component
public class ScopeAuthAdvice {

    @Autowired
    private AuthenticationSupplier supplier;

    private final String SPEL_FLAG = "#";

    @Around("@annotation(scopeAuth)")
    public Object before(ProceedingJoinPoint thisJoinPoint, ScopeAuth scopeAuth) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) thisJoinPoint.getSignature();
        // 获取方法参数
        Object[] args = thisJoinPoint.getArgs();

        // 获取token
        String authToken = getToken(args, scopeAuth.token(), methodSignature.getMethod());
        if (StringUtils.isEmpty(authToken)) {
            throw new AccessDeniedException("100", "没有找到AUTH_TOKEN");
        }


        // 获取请求范围
        Set<String> requestScope = getRequestScope(args, scopeAuth.scope(), methodSignature.getMethod());

        ScopeAuthenticationAdapter adapter = new ScopeAuthenticationAdapter(supplier);
        // 已授权范围
        Set<String> authorizedScope = adapter.identifyPermissionScope(authToken, requestScope);

        if (authorizedScope == null) {
            throw new AccessDeniedException("101", "没有可访问的权限范围");
        }

        // 设置新范围
        setRequestScope(args, scopeAuth.scope(), authorizedScope, methodSignature.getMethod());

        return thisJoinPoint.proceed();
    }

    /**
     * 获取 token
     *
     * @param args
     * @param tokenKey
     * @param method
     * @return
     */
    private String getToken(Object[] args, String tokenKey, Method method) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        tokenKey = StringUtils.isEmpty(tokenKey) ? "AUTH_TOKEN" : tokenKey;

        String token =  request.getHeader(tokenKey);
        if (StringUtils.isEmpty(token) && request.getAttribute(tokenKey) != null){
            token = request.getAttribute(tokenKey).toString();
        }

        // 解析 SPEL 表达式
        if (StringUtils.isEmpty(token) && tokenKey.indexOf(SPEL_FLAG) == 0) {
            token = ParseSPEL.parseMethodKey(tokenKey, method, args, String.class);
        }

        return token;
    }

    /**
     * 设置请的请求范围
     *
     * @param args
     * @param scopeName
     * @param method
     */
    private void setRequestScope(Object[] args, String scopeName, Collection<String> scopeValues, Method method) {

        if (StringUtils.isEmpty(scopeName)) {
            scopeName = "#stationCode";
        }

        // 解析 SPEL 表达式
        if (scopeName.indexOf(SPEL_FLAG) == 0) {
            ParseSPEL.setMethodValue(scopeName, scopeValues, method, args);
        }
    }

    /**
     * 获取请求权限范围
     *
     * @param args
     * @param scopeName
     * @param method
     * @return
     */
    private Set<String> getRequestScope(Object[] args, String scopeName, Method method) {

        Collection<String> scopeList;

        if (StringUtils.isEmpty(scopeName)) {
            scopeName = "#stationCode";
        }

        // 解析 SPEL 表达式
        if (scopeName.indexOf(SPEL_FLAG) == 0) {
            scopeList = ParseSPEL.parseMethodKey(scopeName, method, args, Collection.class);
            if (scopeList == null) {
                return null;
            }
        } else {
            return null;
        }

        Set<String> list = new HashSet<String>();
        list.addAll(scopeList);

//        String splitFlag = ",";
//        Iterator<String> it = scopeList.iterator();
//        String scopeValue = it.next();
//        // 解析 以逗号为分隔
//        if (scopeList.size() == 1 && scopeValue.contains(splitFlag)) {
//            String[] scope = scopeValue.split(splitFlag);
//
//            Collections.addAll(list, scope);
//        } else {
//            list.add(scopeValue);
//
//            while (it.hasNext()) {
//                list.add(it.next());
//            }
//        }

        return list;
    }


}
