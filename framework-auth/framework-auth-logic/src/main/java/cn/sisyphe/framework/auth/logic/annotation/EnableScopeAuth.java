package cn.sisyphe.framework.auth.logic.annotation;

import cn.sisyphe.framework.common.bootstrap.AutoConfigurationImportSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
@Documented
@Import(AutoConfigurationImportSelector.class)
public @interface EnableScopeAuth {
}
