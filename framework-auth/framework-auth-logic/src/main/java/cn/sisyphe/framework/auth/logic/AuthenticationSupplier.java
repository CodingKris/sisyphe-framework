package cn.sisyphe.framework.auth.logic;

import java.util.Set;

/**
 * Created by heyong on 2017/11/23 16:51
 * Description: 权限存储器，可使用redis保存
 * @author heyong
 */
public interface AuthenticationSupplier {

    /**
     * 保存范围
     * @param key
     * @param values
     */
    void importScope(String key, Set<String> values);

    /**
     * 查询范围
     * @param key
     * @return
     */
    Set<String> findScope(String key);
}
