### Framework-Auth-Logic 权限范围过滤

此模块可实现拦截Controller前端请求，实现权限范围的过滤。现实方式如下：

### 使用说明

1.实现权限存储器接口(AuthenticationSupplier),可根据需要存入redis,db或进程中,也可与spring security结合保存在session中

```
@Service
public class AuthSupplier implements AuthenticationSupplier {

    @Override
    public void importScope(String key, Set<String> values) {
        // ... 保存用户权限范围
    }

    @Override
    public Set<String> findScope(String key) {
        // ... 查询用户权限范围
    }
}
``` 

2.在程序主方法上启用权限范围过滤 **@EnableScopeAuth**

3.在需要过滤的Controller上设置 **@ScopeAuth** 注解，并注明请求访问的范围字段，此处为SpEL表达式
此注解另需要获取用户的token，默认取"AUTH_TOKEN"，也可使用SpEL表达式指定某个参数，过滤用的范围参数将重新注入到方法参数中

```
@ScopeAuth(scope = "#user.stationCode", token = "#user.userName")
@PostMapping(value = "/get")
public ResponseResult get(@RequestBody User user) {
    
    // ... user.getStationCode 参数已被过滤，可作为条件进行操作 

    ResponseResult responseResult = new ResponseResult();
    responseResult.put("user", user);
    return responseResult;
}

```

4.导入用户的权限范围

```
Set<String> scopeList = new HashSet<String>();
scopeList.add("5555");

// 导入权限范围
authSupplier.importScope("hypier", scopeList);
```

