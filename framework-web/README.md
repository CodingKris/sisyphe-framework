Sisyphe-Framework-Web
===
网页相关功能模块，统一异常日志配置，统一前端输出类
 
 
### 更新说明
版本|时间|更新内容
:---: |   --- |   :---: 
1.0 |   1. 增加统一前端输出类<br> 2. 增加统一异常日志处理 | 2017-11-22



### 功能说明

* 统一前端输出类 ResponseResult
    * 支持多数据结果输出
    * 支持逻辑异常输出，根据程序逻辑异常返回前端统一提示代码和提示信息
    * 支持认证token下发
    * 支持跨服务端的对象转换

* 统一异常日志配置 DataException
    * 支持统一异常日志收集和按预先配置显示
    * 支持格式化信息输出
    * 支持程序级详细日志输出

### 统一异常日志使用说明

例子详见 [framework-example](framework-example)
    
1.引入web包

```
<dependency>
    <groupId>cn.sisyphe.framework</groupId>
    <artifactId>framework-web</artifactId>
    <version>1.0</version>
</dependency>
```

2.将日志代码与提示说明配置到yml文件中（可由springCloud Config统一管理）,可通过占位符格式化提示语

```
sisyphe:
  exception:
    frontendMessage:
      100: 卡号{0},交易号{1}
      101: 交易失败
```

3.在程序入口导入导入全局异常处理器

```
@Import(GlobalExceptionHandler.class)
```

4.在程序中抛出异常

```
throw new DataException("100", "出错了");  // 1
throw new DataException("100", "{0}-{1}", new String[]{"C001", "SB123"}); // 2
throw new DataException("100", "{0}-{1}", new String[]{"C001", "SB123"}, object1, object2);  // 3
```

参数说明

    1. 第二个参数的文本为程序中日志输出内容,不输出到前端
    2. 第三个参数是格式化内容,会与配置中的文本一起输出到前端
    3. 第四,五个参数或更多是当前运行环境的相关数据,将只在日志中输出,以便跟踪问题