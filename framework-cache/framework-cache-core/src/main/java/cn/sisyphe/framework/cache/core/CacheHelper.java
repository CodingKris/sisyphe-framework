package cn.sisyphe.framework.cache.core;

import cn.sisyphe.framework.cache.core.annotation.CacheMode;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by heyong on 2017/10/20 17:43
 * Description: 缓存助手类
 *
 * @author heyong
 */
@Component
public class CacheHelper implements ApplicationContextAware {

    public static String mode = "redis";

    private static Map<String, S2Cache> cacheMap;


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Map<String, S2Cache> map = applicationContext.getBeansOfType(S2Cache.class);

        cacheMap = new HashMap<>(16);
        map.forEach((key, value) -> cacheMap.put(value.getName().toLowerCase(), value));

    }


    /**
     * 获取配置的缓存类
     * @return
     */
    public static S2Cache cache() {
        S2Cache s2Cache = cacheMap.get(mode.toLowerCase());

        if (s2Cache == null) {
            throw new RuntimeException("Cache Class Not Found");
        }

        return s2Cache;
    }

    /**
     * 获取指定的缓存类
     * @param cacheMode
     * @return
     */
    public static S2Cache cache(CacheMode cacheMode){

        S2Cache s2Cache = cacheMap.get(cacheMode.name().toLowerCase());

        if (s2Cache == null) {
            throw new RuntimeException("Cache Class Not Found");
        }

        return s2Cache;
    }



}
