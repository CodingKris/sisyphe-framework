package cn.sisyphe.framework.cache.core.annotation;

import cn.sisyphe.framework.cache.core.CacheHelper;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by heyong on 2017/10/24 16:59
 * Description: 通过注册bean器 查找注解设置缓存类型
 *
 * @author heyong
 */
@Component
class S2CacheRegistrar implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {

        Map<String, Object> defaultAttrs = metadata.getAnnotationAttributes(EnableS2Cache.class.getName(), true);

        String cacheValue = "value";
        if (defaultAttrs != null && defaultAttrs.containsKey(cacheValue)) {
            // 设置 cache 的类型
            CacheHelper.mode = defaultAttrs.get(cacheValue).toString();
        }
    }
}
