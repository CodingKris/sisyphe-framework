package cn.sisyphe.framework.cache.core.annotation;

/**
 * Created by heyong on 2017/10/24 17:43
 * Description:
 * @author heyong
 */
public enum CacheMode {
    /**
     * redis
     */
    REDIS,
    /**
     * ehcache
     */
    EHCACHE,
}
