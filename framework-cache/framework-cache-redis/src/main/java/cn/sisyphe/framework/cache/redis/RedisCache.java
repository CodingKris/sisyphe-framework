package cn.sisyphe.framework.cache.redis;

import cn.sisyphe.framework.cache.core.S2Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by heyong on 2017/10/23 10:00
 * Description: redis 实现
 *
 * @author heyong
 */
@Component
public class RedisCache implements S2Cache {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


    @Override
    public String getName() {
        return "redis";
    }

    @Override
    public boolean exists(String key) {
        return redisTemplate.hasKey(key);
    }

    @Override
    public Object get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    @Override
    public void set(String key, Object value) {
        set(key, value, null);
    }

    @Override
    public void set(String key, Object value, Long expireTime) {
        if (expireTime == null) {
            redisTemplate.opsForValue().set(key, value);
        } else {
            redisTemplate.opsForValue().set(key, value, expireTime, TimeUnit.SECONDS);
        }
    }

    @Override
    public void remove(String key) {
        if (exists(key)) {
            redisTemplate.delete(key);
        }
    }

    @Override
    public void remove(String... keys) {
        for (String key : keys) {
            if (exists(key)) {
                remove(key);
            }
        }
    }

    @Override
    public long increment(String key, long value) {

        return redisTemplate.opsForValue().increment(key, value);
    }

    @Override
    public void hashPush(String key, String hashKey, Object value) {
        hashPush(key, hashKey, value, null);
    }

    @Override
    public void hashPush(String key, String hashKey, Object value, Long expireTime) {
        HashOperations<String, String, Object> hashOperations = redisTemplate.opsForHash();
        hashOperations.put(key, hashKey, value);

        if (expireTime != null) {
            redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
        }
    }

    @Override
    public Object hashPop(String key, String hashKey) {
        return redisTemplate.opsForHash().get(key, hashKey);
    }

    @Override
    public boolean hashHasKey(String key, String hashKey) {
        return redisTemplate.opsForHash().hasKey(key, hashKey);
    }

    @Override
    public void hashRemove(String key, String hashKey) {
        redisTemplate.opsForHash().delete(key, hashKey);
    }

    @Override
    public Set<Object> hashKeys(String key) {
        return redisTemplate.opsForHash().keys(key);
    }

    @Override
    public List<Object> hashValues(String key) {
        return redisTemplate.opsForHash().values(key);
    }

    @Override
    public long hashIncrement(String key, String hashKey, long value) {
        return redisTemplate.opsForHash().increment(key, hashKey, value);
    }
}
