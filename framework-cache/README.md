Sisyphe-Framework-Cache 缓存框架
===
对多种缓存方式进行统一封装,调用方式简单,切换方式灵活


### 更新说明
版本|时间|更新内容
:---: |   --- |   :---: 
1.0 |   1. 增加cache-core 接口层<br>2. 增加redis实现 | 2017-10-24
1.0.1   | 1.增加redis 注解操作方式  | 2017-11-13

### cache-core 接口层说明
    - CacheHelper   缓存调用类，接口实现类加载
    - S2Cache   缓存基础接口
        - CacheMode 缓存类型枚举
        - EnableS2Cache 开启缓存注解
        - S2CacheRegistrar  缓存类型设置操作
    - spring.factories  配置自动加载的类
    
### cache-redis 实现类说明
    - RedisCache    redis缓存实现类，实现至S2Cache
        - RedisConfiguration    redis缓存配置类，配置key/value的序列化方式，注解式缓存的key生成规则
    - spring.factories  配置自动加载的类，上两个文件必须要加入到此配置中才能被接口层识别 
     
### 如何扩展缓存实现类
1. 在framework-cache下新建实现模块
2. 引入 framework-cache-core 包
3. 新建实现类并实现S2Cache接口
4. 新建spring.factories文件，将需要自动注入（配置类，自重启类等）的类加入至此文件

### 如何使用缓存(redis)
1.在项目中引入Cache现实类包

```
<dependency>
    <groupId>cn.sisyphe.framework</groupId>
    <artifactId>framework-cache-redis</artifactId>
    <version>1.0</version>
</dependency>
```

2.在启动类上开启缓存注解 **@EnableS2Cache**

3.1.使用代码调用缓存操作类

````
// 设置缓存（无过期时间）
CacheHelper.cache().set("name", "abc");
// 设置缓存（有过期时间）
CacheHelper.cache().set("name", "abc", 1800L);
// 读取缓存
CacheHelper.cache().get("name");
// 删除缓存
CacheHelper.cache().remove("name");
````

3.2.使用注解(@Cacheable,@CacheEvict)调用缓存操作类

注意：
* 只适用于 **public** 方法，且只能由其它类调用本方法有效，本类互调无效
* 全局默认过期时间10800秒，可通过 “sisyphe.s2cache.expireTime” 在配置文件中自定义

````
/**
 * 自动保存缓存
 * @param userName
 * @return
 */
@Cacheable(value = "user")
public User getUserInfo(String userName) {
    // 取数据库 ...
    return new User(userName, "CQ");
}

/**
 * 自动更新缓存
 * @param userName
 */
@CachePut(value = "user")
public User saveUserInfo(String userName) {
    // 存数据库 ...
}

/**
 * 自动删除缓存
 *
 * @param userName
 */
@CacheEvict(value = "user", allEntries = true)
@RequestMapping(path = "/del", method = RequestMethod.POST)
public void deleteUserInfo(@RequestParam String userName) {
    // 删除数据库 ...

}

````