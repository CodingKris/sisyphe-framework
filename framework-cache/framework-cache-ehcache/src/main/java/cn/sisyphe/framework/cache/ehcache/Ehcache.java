package cn.sisyphe.framework.cache.ehcache;

import cn.sisyphe.framework.cache.core.S2Cache;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * Created by heyong on 2017/10/24 14:14
 * Description:
 * @author heyong
 */
@Component
public class Ehcache implements S2Cache{

    @Override
    public String getName() {
        return "ehcache";
    }

    @Override
    public boolean exists(String key) {
        return false;
    }

    @Override
    public Object get(String key) {
        return null;
    }

    @Override
    public void set(String key, Object value) {

    }

    @Override
    public void set(String key, Object value, Long expireTime) {

    }

    @Override
    public void remove(String key) {

    }

    @Override
    public void remove(String... keys) {

    }

    @Override
    public long increment(String key, long value) {
        return 0;
    }

    @Override
    public void hashPush(String key, String hashKey, Object value) {

    }

    @Override
    public void hashPush(String key, String hashKey, Object value, Long expireTime) {

    }

    @Override
    public Object hashPop(String key, String hashKey) {
        return null;
    }

    @Override
    public boolean hashHasKey(String key, String hashKey) {
        return false;
    }

    @Override
    public void hashRemove(String key, String hashKey) {

    }

    @Override
    public Set<Object> hashKeys(String key) {
        return null;
    }

    @Override
    public List<Object> hashValues(String key) {
        return null;
    }

    @Override
    public long hashIncrement(String key, String hashKey, long value) {
        return 0;
    }
}
