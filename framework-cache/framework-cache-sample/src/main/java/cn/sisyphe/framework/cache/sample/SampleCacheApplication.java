package cn.sisyphe.framework.cache.sample;

import cn.sisyphe.framework.cache.core.CacheHelper;
import cn.sisyphe.framework.cache.core.annotation.EnableS2Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by heyong on 2017/10/24 10:27
 * Description:
 *
 * @author heyong
 */
@SpringBootApplication
@EnableS2Cache
@EnableSwagger2
public class SampleCacheApplication implements CommandLineRunner {

    public static void main(String[] args) {
        new SpringApplicationBuilder(SampleCacheApplication.class).run(args);
    }

    @Autowired
    private BusinessController businessController;

    @Override
    public void run(String... strings) throws Exception {

        cacheCodeCall();
    }

    /**
     * 代码式调用缓存
     */
    private void cacheCodeCall() {
        CacheHelper.cache().set("abc", new User("heyong", "CQ"));
        for (int i = 0; i < 100; i++) {
            System.err.println(CacheHelper.cache().get("abc"));

        }
    }


}
