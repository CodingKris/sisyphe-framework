package cn.sisyphe.framework.cache.sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by heyong on 2017/10/26 18:46
 * Description: 注解式调用缓存
 *
 * @author heyong
 */
@RestController
@RequestMapping("business")
public class BusinessController {

    private static final Logger log = LoggerFactory.getLogger(BusinessController.class);

    /**
     * 自动保存缓存
     *
     * @param userName
     * @return
     */
    @Cacheable(value = "user")
    @RequestMapping(path = "/get", method = RequestMethod.POST)
    public User getUserInfo(@RequestParam String userName) {
        // 取数据库 ...

        User user = new User(userName, "");
        log.info("新用户：{}", user);
        return user;
    }

    /**
     * 自动保存缓存-带条件
     *
     * @param userName
     * @return
     */
    @Cacheable(value = "user", condition = "#userName=='sis'")
    @RequestMapping(path = "/getCondition", method = RequestMethod.POST)
    public User getUserInfoByCondition(@RequestParam String userName) {
        // 取数据库 ...

        User user = new User(userName, "");
        log.info("新用户：{}", user);
        return user;
    }

    /**
     * 自动更新缓存
     *
     * @param userName
     */
    @CachePut(value = "user")
    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public void saveUserInfo(@RequestParam String userName) {
        // 存数据库 ...

    }

    /**
     * 自动删除缓存
     *
     * @param userName
     */
    @CacheEvict(value = "user", allEntries = true)
    @RequestMapping(path = "/del", method = RequestMethod.POST)
    public void deleteUserInfo(@RequestParam String userName) {
        // 删除数据库 ...

    }

}
