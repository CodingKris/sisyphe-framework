package cn.sisyphe.framework.cache.sample;

import java.io.Serializable;

/**
 * Created by heyong on 2017/10/26 15:53
 * Description:
 * @author heyong
 */
public class User implements Serializable {
    private String userName;
    private String address;
    private Long time;

    public User() {
    }

    User(String userName, String address) {
        this.userName = userName;
        this.address = address;

        this.time = System.currentTimeMillis();
    }


    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", address='" + address + '\'' +
                ", time=" + time +
                '}';
    }
}